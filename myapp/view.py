from .app import app, db
from flask import render_template, redirect, url_for, request
from .models import *
from .form import *
from flask_login import login_user, current_user, logout_user, login_required

def rechercheStyle(css):
	return url_for('static', filename=css)

@app.route('/')
def home():
	return render_template(
		"home.html",
		title="Tiny Amazon",
		books=get_sample()
	)

@app.route("/authors")
def authors():
	auteurs = get_authors()
	return render_template(
				"authors.html",
				title="Liste de tous les auteurs ",
				authors=auteurs,
				style=rechercheStyle("authors.css")
	)

@app.route("/author/<int:id>")
def one_author(id):
	auteur = get_author(id)
	return render_template(
				"home.html",
				title="Livre de "+auteur.name,
				books=auteur.books
	)

@app.route('/edit/author/<int:id>/')
@login_required
def edit_author(id=None):
	a = get_author(id=id)
	f = AuthorForm(id=a.id, name=a.name)
	return render_template(
		"edit-author.html",
		author=a,form=f
	)

@app.route('/edit/author/<int:id>/', methods=("POST",))
def save_author(id=None):
	f = AuthorForm()
	a = get_author(id)
	id = int(f.id.data)
	if f.validate_on_submit():
		a.name = f.name.data
		db.session.commit()
		return redirect(url_for("one_author",id=id))
	return render_template(
		"edit-author.html",
		author=a,form=f
	)

@app.route('/find/author/')
def findAuthorForm():
	f = FindForm()
	return render_template(
		"find.html",
		title="Rechercher un auteur :",
		form=f
	)

@app.route('/find/author/', methods=("POST",))
def findedAuthor():
	f = FindForm()
	res = get_idAuthor(f.name.data)
	if len(res)==1:
		return redirect(url_for("one_author",id=res[0].id))
	elif len(res)==0:
		return redirect(url_for("findAuthorForm"))
	return render_template(
		"finded.html",
		liste=res,
		title="Recherche auteurs : "+f.name.data,
		page="/author",
		affiche="name"
	)

@app.route('/find/book/')
def findBookForm():
	f = FindForm()
	return render_template(
		"find.html",
		title="Rechercher un livre :",
		form=f
	)

@app.route('/find/book/', methods=("POST",))
def findedBook():
	f = FindForm()
	res = get_idBook(f.name.data)
	if len(res)==1:
		return redirect(url_for("one_book",id=res[0].id))
	elif len(res)==0:
		return redirect(url_for("findBookForm"))
	return render_template(
		"finded.html",
		liste=res,
		title="Recherche livres : "+f.name.data,
		page="/book",
	)

@app.route("/login/", methods=("GET","POST",))
def login():
	f = LoginForm()
	if not f.is_submitted():
		f.next.data = request.args.get("next")
	elif f.validate_on_submit():
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			next = f.next.data or url_for("home")
			return redirect(next)
	return render_template("login.html",form=f)

@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for("home"))


@app.route('/add/author')
#@login_required
def addAuthor():
	f = AddAuthorForm()
	return render_template(
		"add-author.html",
		title="Add Author",
		form = f
	)

@app.route('/add/author', methods=("POST",))
#@login_required
def addedAuthor():
	f = AddAuthorForm()
	a = add_author(name = f.name.data)
	return render_template(
		"add-author.html",
		title="Add Author",
		form = f
	)

@app.route("/favoris/")
@login_required
def favoris():
	return render_template(
					"home.html",
					title="Vos favoris",
					books=get_favoris(current_user.username)
					)

@app.route("/addFav/<int:id>")
@login_required
def pageAjoutFavoris(id):
	ajoutFavoris(current_user.username, id)
	return redirect(url_for("home"))

@app.route("/book/<int:id>/")
def one_book(id):
	return render_template(
		"one_book.html",
		style = rechercheStyle("one_book.css"),
		book = get_book(id)
	)
