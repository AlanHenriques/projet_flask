from .app import db, login_manager
from flask_login import UserMixin
from sqlalchemy import func


association_table_like = db.Table('association', db.metadata,
    db.Column('left_id', db.String(50), db.ForeignKey('user.username'), primary_key=True),
    db.Column('right_id', db.Integer, db.ForeignKey('book.id'), primary_key=True)
)

class User(db.Model, UserMixin):
	__tablename__ = "user"
	username = db.Column(db.String(50), primary_key=True)
	password = db.Column(db.String(64))
	maListe = db.relationship("Book", secondary=association_table_like)

	def get_id(self):
		return self.username

class Author(db.Model):
	__tablename__ = "author"
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100))

	def __repr__(self):
		return "<Author (%d) %s>" % (self.id, self.name)

class Book(db.Model):
	__tablename__ = "book"
	id = db.Column(db.Integer, primary_key=True)
	price = db.Column(db.Float)
	title = db.Column(db.String(120))
	url = db.Column(db.String(250))
	img = db.Column(db.String(90))
	author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
	author = db.relationship("Author",
				backref=db.backref("books", lazy = "dynamic"))

	def __repr__(self):
		return "<Books (%d) %s>" % (self.id , self.title)

def get_sample():
	return Book.query.all()

def add_author(name):
	idMax = db.session.query(func.max(Author.id)).scalar()
	a = Author(id=idMax+1,
			 name=name)
	db.session.add(a)
	db.session.commit()

def get_author(id):
	return Author.query.get_or_404(id)

def get_authors():
	return Author.query.all()

def get_idAuthor(txt):
	res = Author.query.filter(Author.name.like("%"+txt+"%")).all()
	return res

def get_book(id):
	return Book.query.get_or_404(id)

def get_idBook(txt):
	res = Book.query.filter(Book.title.like("%"+txt+"%")).all()
	return res

@login_manager.user_loader
def load_user(username):
	return User.query.get(username)

def get_favoris(username):
	return User.query.get(username).maListe

def ajoutFavoris(username, idBook):
    commande = "INSERT INTO association VALUES ('%s', %i)" % (username, idBook)
    db.session.execute(commande)
    db.session.commit()
