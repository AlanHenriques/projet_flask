from .models import User, Author, Book
from hashlib import sha256

from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, validators
from wtforms.validators import DataRequired

class AuthorForm(FlaskForm):
	id = HiddenField("id")
	name = StringField("nom",
			validators=[DataRequired(),
			validators.Length(
				min=2,
				max=25,
				message="Le nom doit avoir entre 2 et 25 caractères.")]
	)

class FindForm(FlaskForm):
	name = StringField("nom",
			validators=[DataRequired(message="Il doit y avoir quelque chose pour pouvoir le rechercher.")]
	)

class LoginForm(FlaskForm):
	username = StringField("Pseudo")
	password = StringField("Mot de passe")
	next = HiddenField()

	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		if user!=None and user.password==sha256(self.password.data.encode()).hexdigest():
			return user
		return None

class AddAuthorForm(FlaskForm):
	name = StringField("Nom Auteur",
			validators=[DataRequired(),
			validators.Length(
				min=2,
				max=25,
				message="Le nom doit avoir entre 2 et 25 caractères.")]
	)