from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os.path
from flask_bootstrap import Bootstrap
from flask_login import LoginManager

app=Flask(__name__)
app.debug=True
Bootstrap(app)
app.config["BOOTSTRAP_SERVE_LOCAL"] = True

def mkpath(p):
	return os.path.normpath(
		os.path.join(
			os.path.dirname(__file__),p))

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config["SQLALCHEMY_DATABASE_URI"] = ("sqlite:///"+mkpath("../myapp.db"))
app.config["SECRET_KEY"]='5a1e786d-6e40-47ad-ad9e-e0cc2c5cc7cf'

db = SQLAlchemy(app)

login_manager = LoginManager(app)
login_manager.login_view="login"
