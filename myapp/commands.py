import click
from .app import app, db

@app.cli.command()
def syncdb():
	""" crée les tables de la BD"""
	db.create_all()

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
	"""insert à la BD les données du fichier paramètre"""
	db.create_all()

	import yaml
	books = yaml.load(open(filename))

	from .models import Author, Book

	authors={}
	for b in books:
		a = b["author"]
		if a not in authors:
			o = Author(name=a)
			db.session.add(o)
			authors[a]=o
	db.session.commit()

	for b in books:
		a=authors[b["author"]]
		o = Book( price = b["price"],
				  title = b["title"],
				  url = b["url"],
				  img = b["img"],
				  author_id=a.id)
		db.session.add(o)
	db.session.commit()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
	"""ajoute l'user à la BD"""
	from .models import User
	from hashlib import sha256

	u = User(username=username,
			 password=sha256(password.encode()).hexdigest())
	db.session.add(u)
	db.session.commit()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def passwd(username, password):
	"""change le mot de passe de username"""
	from .models import User
	from hashlib import sha256

	u = User.query.get(username)
	u.password = sha256(password.encode()).hexdigest()
	db.session.commit()